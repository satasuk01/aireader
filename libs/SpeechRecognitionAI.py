import speech_recognition as sr
import time
import pyaudio

from pygame import mixer

# Initialize recognizer class (for recognizing the speech)
r = sr.Recognizer()

# Reading Audio file as source
# listening the audio file and store in audio_text variable

def recognize():
    with sr.Microphone(device_index=0) as source:
        print(sr.Microphone.list_microphone_names())
    #     r.adjust_for_ambient_noise(source)
        print("Record is starting ...")
    #     audio_text = r.listen(source)
        audio_text = r.record(source, duration = 5)
        print("Finish")
        
        
    # recoginize_() method will throw a request error if the API is unreachable, hence using exception handling
        try:
            
            # using google speech recognition
            text = r.recognize_google(audio_text)
            print('Converting audio transcripts into text ...')
            print('Hearing: '+text)
            
            return text
         
        except:
             print('Sorry.. run again...')
