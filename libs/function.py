from dearpygui import core, simple
from datetime import datetime
from libs import SpeechRecognitionAI as sr
import re
from gtts import gTTS
from pygame import mixer
import os


class AI():
    def __init__(self, word=''):
        self.output_str = word
        mixer.init()

    def start(self):
        core.set_value("O", out("AI: Hello User, Press 'REC' Button to start!\n"))
        self.speak("AI: Hello User, Press 'REC' Button to start!\n")

    def setVolume(self):
        amount = core.get_value("Volume")
        mixer.music.set_volume(amount)

    def speak(self, word):
        speakGTTS = gTTS(text=word[3:], lang='en', slow=False) # cut word AI
        speakGTTS.save('speak.mp3')

        # Play sound
        mixer.music.load('speak.mp3')
        mixer.music.play()
        os.remove('speak.mp3')

    def output(self, word):
        print("SPEAK: "+word)
        self.speak(word)
        res = self.output_str + out(word) + '\n'
        core.set_value("O", res)
        self.output_str = res

    def hear(self):
        '''Function for hearing word'''
        self.output('AI: Start Recording...')
        core.configure_item("REC", enabled=False)
        word = sr.recognize()
        core.configure_item("REC", enabled=True)
        try:
            self.output('AI: I heard "' + word + '"')

        except TypeError:
            self.output("AI: I can't understand what you said.")
        return word


def time():
    now = datetime.now()
    time = now.strftime("[%H:%M:%S]")
    return time

def out(word):
    return time() + ' ' + re.sub("(.{49})", "\\1\n", word, 0, re.DOTALL)


def output(OUTPUT_STR, word):
    res = OUTPUT_STR + out(word) + '\n'
    core.set_value("O", res)
    return res


def save_callback(sender, data):
    print("Save Clicked")
