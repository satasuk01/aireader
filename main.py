from dearpygui import core, simple
from libs import function as fn

# Create AI
AI = fn.AI(fn.out("AI: Hello User, Press 'REC' Button to start!\n"))


# Setting Window
core.set_theme("Light")    
core.set_main_window_size(500,700)
core.set_style_window_padding(30,30)
with simple.window("Main"):
    
    # Set window
    core.set_primary_window("Main", True)
    core.set_main_window_pos(0,0)
    core.set_main_window_resizable(False)
    core.set_main_window_title("AI Reader")
    
    # Create Logo of the app
    core.add_image("AIReader","./assets/AIReader.png", width = 440,height = 70, parent="Main")
    core.add_separator(parent="Main")
    core.add_spacing(count=5, parent="Main")
    
    # Create output Zone
    core.add_input_text("O",  multiline=True, readonly=True, width = 440, height = 200)
    AI.start()
    # core.set_value("O", fn.out(" AI: Hello User, Press 'REC' Button to start!"))
    # OUTPUT_STR.output("AI: Hello Again")
    
    core.add_spacing(count=5, parent="Main")
    # core.add_button("REC", width=440, height=50, callback=lambda x: OUTPUT_STR.output("AI: Clicked!"), parent="Main")
    core.add_button("REC", width=440, height=120, callback=AI.hear, parent="Main")
    core.add_spacing(count=5, parent="Main")

    core.add_slider_float("Volume", parent="Main", callback=AI.setVolume, default_value=1., max_value=1.)

core.start_dearpygui()
